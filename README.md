The Docker setup for PHP applications using PHP7-FPM and Nginx described in http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm

## Instructions
1. Checkout the repository
2. Create a record in your `hosts` file to point `php7-docker.local` to your Docker environment
3. Run `docker-compose up`
4. Navigate to php7-docker.local:7777 in a browser

That's it! You have your local PHP setup using Docker