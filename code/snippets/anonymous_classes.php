<?php
/**
 * Anonymous Classes
 */

$deathstar = new class {
	public function selfdestruct() {
		return "Boom!";
	}
};
echo print_r($deathstar, true);
echo $deathstar->selfdestruct();