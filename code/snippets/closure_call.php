<?php
/**
* Closure::call()
*/
class Deathstar {
	private function selfdestruct() {
		return 'Oops!';
	}
}

$selfdestructCallback = function() {
	return $this->selfdestruct();
};
//PHP5 style
$bind = $selfdestructCallback->bindTo(new Deathstar,'Deathstar');
echo $bind() . "<br><br>";
//PHP7 style
echo $selfdestructCallback->call(new Deathstar) . "<br>";