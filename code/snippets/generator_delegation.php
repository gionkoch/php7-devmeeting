<?php
function gen1() {
	yield 2;
}
function gen2() {
	$prices = array(5,12);
	yield from gen1();
	foreach($prices as $val) {
		yield $val;
	}
}
foreach (gen2() as $val) {
	echo "<p>Price: " . $val . "</p>";
}