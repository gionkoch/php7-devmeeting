<?php
$genReturn = (function() {
	$prices = array(2,5,12);
	foreach($prices as $val) {
		yield $val;
	}
	return array_sum($prices);
})();
foreach ($genReturn as $val) {
	echo "<p>Price: " . $val . "</p>";
}
echo "<p><strong>Total: " . $genReturn->getReturn() . "</strong></p>";