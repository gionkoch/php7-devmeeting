<?php

$array = ['foo'=>'bar'];

//old style
echo isset($array['foo']) ? $array['foo'] : 'does not exist';
echo "<br>";
echo "<br>";

//PHP 7 style
echo $array['foo'] ?? 'does not exist';
echo "<br>";